import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class WorkWithArrays {
    public static void main(String[] args) {
        int[] randomArray = new int[8];
        Arrays.setAll(randomArray, i -> new Random().nextInt(10) + 1);
        System.out.print(Arrays.toString(randomArray));
        System.out.println();
        int[] sortedArray = randomArray.clone();
        Arrays.sort(sortedArray);
        if (Arrays.equals(randomArray, sortedArray)) {
            System.out.println("Array is ascending sorted");
        } else {
            System.out.println("Array is not ascending sorted");
        }
        IntStream.range(0, randomArray.length).filter(i -> i % 2 == 1).forEach(i -> randomArray[i] = 0);
        System.out.print(Arrays.toString(randomArray));
    }
}
